import java.util.Arrays;
import java.util.Scanner;
public class Main {
        public static void main(String[] args) {

            System.out.println("   TASK 1     ");
            //1.Solution
            System.out.println("---------Fist solution-------");
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter a Sentence: ");
            String word = scanner.nextLine();
            String vowels = "aeiou";
            int countlower = 0;
            int countupper = 0;
            for (int i = 0; i < word.length(); i++) {
                for (int k = 0; k < vowels.length(); k++) {
                    if (word.charAt(i) == vowels.toLowerCase().charAt(k)) {
                        countlower++;
                    }
                    if (word.charAt(i) == vowels.toUpperCase().charAt(k)) {
                        countupper++;
                    }

                }
            }
            System.out.println("Lower case count: " + countlower);
            System.out.println("Upper case count: " + countupper);


            //2.Solution
            System.out.println("---------Solution 2-----");
            System.out.print("Enter a sentence: ");
            String word2 = scanner.nextLine();
            System.out.println("All Vowels Counts: " + VowelCount(word2));




            System.out.println("  TASK 2     ");
            System.out.print("Input a word: ");
            String x = scanner.nextLine();
            System.out.println("Reverse: " + ReverseString(x));





            System.out.println("      Task 3    ");
            System.out.print("Input a word: ");
            String forPolindrome = scanner.nextLine();
            System.out.println("Polindrome word: "+addPalindrome(forPolindrome));


            //METODS



        }
        public static int VowelCount(String word1) {
            String vowels ="aeoiuAEOIU";
            int count = 0;
            for (int i = 0; i < word1.length(); i++)
                for (int k = 0; k <  vowels.length(); k++) {
                    if (word1.charAt(i)==vowels.charAt(k)){
                        count++;
                    }

                }
            return count;
        }

        public static String ReverseString(String x){
            char[] arr = x.toCharArray();
            int start = 0;
            int end = x.length()-1;
            while (start<end) {
                char temp = arr[start];
                arr[start] = arr[end];
                arr[end] = temp;
                start++;
                end--;

            } return Arrays.toString(arr);
        }
        public static String addPalindrome(String s) {
            char[] arr1 = s.toCharArray();
            int start = 0;
            int end = arr1.length - 1;
            while (start < end) {
                char temp = arr1[start];
                arr1[start] = arr1[end];
                arr1[end] = temp;
                start++;
                end--;

            }
            StringBuilder stringBuilder = new StringBuilder();
            for (char polindrome1: arr1){
                stringBuilder.append(polindrome1);
            }
            String polindrome = s + stringBuilder.toString();
            return polindrome;
        }


    }

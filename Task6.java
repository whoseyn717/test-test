import java.util.*;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Queue<Customer> Linked = new LinkedList<>();
        Queue<Customer> ArraydeQ = new ArrayDeque<>();
        Queue<Customer> Priority = new PriorityQueue<>(Comparator.comparingInt(Customer::getAge));

        Scanner scannner = new Scanner(System.in);
        System.out.println("              Customer Queue Operations ");
        while (true) {
            System.out.println("Enter customer information or type 'exit' to quit.");
            System.out.print("Customer Name: ");
            String name = scannner.nextLine();
            if (name.equalsIgnoreCase("exit")) {
                break;
            }
            System.out.print("Customer Surname: ");
            String surname = scannner.nextLine();

            System.out.print("Customer Age: ");
            int age = scannner.nextInt();
            scannner.nextLine();

            Customer customer = new Customer(name, surname, age);
            Linked.add(customer);
            ArraydeQ.add(customer);
            Priority.add(customer);
            System.out.println("Customer successfully added");
            System.out.println();

        }

        System.out.println("\nLinkedList Queue:");
        queueProcess(Linked);

        System.out.println("\nArrayDeque Queue:");
        queueProcess(ArraydeQ);

        System.out.println("\nPriorityQueue Queue:");
        queueProcess(Priority);

    }

    public static void queueProcess(Queue <Customer> queue){
        while (!queue.isEmpty()){
            Customer customer = queue.poll();
            System.out.println("Name: "+customer.getName());
            System.out.println("Surname: "+customer.getSurname());
            System.out.println("Age: "+customer.getAge());
            System.out.println("Customers count in the queue: "+queue.size());
            System.out.println();
        } if (queue.isEmpty()){
            System.out.println("No customers in the Queue");
        }
    }

}
class Customer {


    private String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Customer(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}

